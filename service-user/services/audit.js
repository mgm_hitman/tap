const axios = require('axios').default;
const log = require('debug')('tap:services:audit');
const config = require('../config');

const stringify = require('../utils/stringify');

exports.create = async ({userId, token}) => {
  log('Starting call to service-audit for userId: ', userId);

  const result = await axios.post(
    `${config.services.audit}/api/audit/register`, 
    { userId },
    {
      headers: {
        'Authorization': `Bearer ${token}`,
      },
    })
    .catch((err) => {
      log('Error in AuditService create while calling to remote service cause:', err);
      return {error: { code: 409, message: 'Conflict storing audit information'}};
    });

  if(result) {
    if (result.error) {
      return result.error;
    }
  } else {
    log('Empty response from AuditService create service call');
    return {error: { code: 501, message: 'Internal Server Error'}};
  }  
  log('Audit record registered Ok for userId:', userId);

  return result;
};

