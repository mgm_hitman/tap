const User = require('../controllers/user');

module.exports = (api) => {
  api.route('/api/user/status').post(User.getUserStatus);
  api.route('/api/user/updatePlan').post(User.updatePlan);
};
