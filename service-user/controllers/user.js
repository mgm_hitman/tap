const log = require('debug')('tap:controllers:user');
const User = require('../models/user');
const AuditService = require('../services/audit');
const stringify = require('../utils/stringify');

exports.getUserStatus = async (req, res) => {
  const { cuils } = req.body;
  //TODO Fix to use SYSTEM credentials based token instead of user token
  const { user, token } = req;
  log('getUserStatus starting with cuils: ', cuils);
  // return res.status(200).json({ user });
  const resultUser = await User.find({ cuil: { $in: cuils } })
    .select({ email: 1, cuil: 1, status: 1 })
    .catch((e) => {
      log('Error in getUserStatus while retrieving User status, cause: ', e);
      return { error: { code: 400, message: 'Bad request' } };
    });

  if (resultUser.error) {
    return res.status(resultUser.error.code).json({ message: resultUser.error.message });
  }

  const resultAudit = await AuditService.create({ userId: user.id, token })
    .catch((e) => {
      log('Error in getUserStatus while creating audit record, cause: ', e);
      return { error: { code: 409, message: 'Conflict storing audit information' } };
    });

  if (resultAudit) {
    if (resultAudit.error) {
      return res.status(resultAudit.error.code).json({ message: resultAudit.error.message });
    }
    log('Response from AuditService.create:', resultAudit);
  } else {
    return { error: { code: 500, message: 'Internal Server error' } };
  }

  return res.status(200).json(resultUser);
};

exports.updatePlan = async (req, res) => {
  const { role } = req.user.role;
  const { userId, plan } = req.body;

  log(`updatePlan starting with caller user's role: ${role} and req.body: `, stringify(req.body));
  if (role !== 'admin') {
    const message = 'Admin role es required for this operation';
    log(message);
    return res.status(403).json({ message });
  }

  const criteria = { _id: userId };

  const resultUser = await User.update(criteria, { plan: { $push: plan } })
    .catch((e) => {
      log(`Error in updatePlan while updating plan to ${plan}, cause: `, e);
      return { error: { code: 400, message: 'Bad request' } };
    });

  if (resultUser.error) {
    return res.status(resultUser.error.code).json({ message: resultUser.error.code });
  }

  return res.status(200).json(resultUser);
};
