const log = require('debug')('tap:controllers:auth');
const jwt = require('jsonwebtoken');
const config = require('../config');
const User = require('../models/user');
const stringify = require('../utils/stringify');

const findUser = async (email, password) => {
  const user = await User.findOne({ email, password })
    .catch((e) => {
      log(`Error in findUser while retrieving User with email: ${email}, cause: `, e);
      return { error: { code: 400, message: 'Error retrieving user data with provided credentials' } };
    });
  return user;
};

const signToken = (tokenData) => {
  const expiration = Number(config.jwt.expiration);
  const exp = Math.floor(Date.now() / 1000) + expiration;
  const signOptions = { algorithm: 'HS512' };

  return jwt.sign({ ...tokenData, exp }, config.jwt.secret, signOptions);
};

exports.login = async (req, res) => {
  const { username, password } = req.body;
  log('Login started');
  const user = await findUser(username, password);
  log('Login retrieved user: ', stringify(user));

  if (user) {
    if (user.error) {
      return res.status(user.error.code).json({ message: user.error.message });
    }

    if (user.active) {
      const confirmedUser = {
        id: user.id,
        email: user.email,
        role: user.role,
      };

      const token = signToken(confirmedUser);
      return res.json({ status: 'ok', msg: 'Login Ok', token });
    }
    return res.status(401).json({ message: 'User deactivated. Contact TAP admin to reactivate' });
  }

  return res.status(401).json({ status: 'fail', msg: 'Unauthorized' });
};

exports.logout = (req, res) => {
  log('Logout called');
  res.json({ status: 'ok, ', msg: 'Logout Ok', token: 'invalid' });
};
