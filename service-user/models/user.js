const mongoose = require('mongoose');

const { Schema } = mongoose;
const bcrypt = require('mongoose-bcrypt');
const timestamps = require('mongoose-timestamp');
const mongooseStringQuery = require('mongoose-string-query');

// eslint-disable-next-line no-unused-vars
const log = require('debug')('services:controllers:user');

const UserSchema = new Schema(
  {
    email: {
      type: String,
      lowercase: true,
      trim: true,
      index: true,
      unique: true,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    role: {
      type: String,
      trim: true,
      required: true,
    },
    firstname: {
      type: String,
      trim: true,
      required: true,
    },
    lastname: {
      type: String,
      trim: true,
      required: true,
    },
    cuil: {
      type: String,
      trim: true,
      required: true,
    },
    plan: {
      type: Number,
      default: 1000,
      required: true,
    },
    status: {
      type: Number,
      default: 1,
      required: true,
    },
    active: {
      type: Boolean,
      default: true,
    },
  },
  { collection: 'users' },
);

// eslint-disable-next-line func-names
UserSchema.pre('save', function (next) {
  if (!this.isNew) {
    next();
  }

  next();
});

// eslint-disable-next-line func-names
UserSchema.pre('findOneAndUpdate', function (next) {
  // eslint-disable-next-line no-underscore-dangle
  if (!this._update.recoveryCode) {
    return next();
  }

  return next();
});

UserSchema.plugin(timestamps);
UserSchema.plugin(bcrypt);
UserSchema.plugin(mongooseStringQuery);

UserSchema.index({ email: 1, cuil: 1, plan: 1 });

module.exports = mongoose.model('User', UserSchema);
