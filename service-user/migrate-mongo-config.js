// In this file you can configure migrate-mongo
require('dotenv').config({ path: './.env' });

module.exports = {
  mongodb: {
    // TODO Change (or review) the url to your MongoDB:
    url: 'mongodb://tap-db:27017/',

    // TODO Change this to your database name:
    databaseName: process.env.DB_NAME,

    options: {
      user: process.env.DB_USER,
      password: process.env.DB_PASS,
      useNewUrlParser: true, // removes a deprecation warning when connecting
      useUnifiedTopology: true, // removes a deprecating warning when connecting
      connectTimeoutMS: 360000, // increase connection timeout to 1 hour
      socketTimeoutMS: 360000, // increase socket timeout to 1 hour
      //   connectTimeoutMS: 3600000, // increase connection timeout to 1 hour
      //   socketTimeoutMS: 3600000, // increase socket timeout to 1 hour
    },
  },

  // The migrations dir, can be an relative or absolute path. Only edit this when really necessary.
  migrationsDir: 'migrations',

  // The mongodb collection where the applied changes are stored.
  // Only edit this when really necessary.
  changelogCollectionName: 'changelog',
};
