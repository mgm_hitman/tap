TAP Microservices using Express, Mongoose, Nginx and Docker compose
================================


Usage
-----
Create first a .env files with the following variables:

```
DEBUG=tap*

APPLICATION_URL=http://localhost

# LOGGER
SERVICE_HOST=localhost
SERVICE_PORT=7777

# DB
DB_HOST=tap-db
DB_PORT=27017
DB_USER=user_tap
DB_PASS=pass_tap
DB_NAME=tap_db

# AUTH
JWT_TOKEN_EXPIRATION=18000
JWT_SECRET_SIGNING_KEY=Super clave secreta para el firmado de los tokens JWT ;)

# SERVICE BASE URLS
SERVICE_USER=http://service-user:7777
SERVICE_AUDIT=http://service-audit:7777
SERVICE_AUTH=http://service-auth:7777
```

Then launch docker-compose from project path:

```bash
[PROJECT_PATH]> docker-compose up --force-recreate
```

That should download docker images and then launch 4 containers:

- tap-db
- tap-nginx
- service-user
- service-audit
- service-auth

Feel free to check if all of them are running as expected (use a new shell windows):

```bash
[PROJECT_PATH]> docker-compose ps

    Name                   Command               State                    Ports                  
-------------------------------------------------------------------------------------------------
service-audit   docker-entrypoint.sh npm start   Up      0.0.0.0:32802->7777/tcp                 
service-auth    docker-entrypoint.sh npm start   Up      0.0.0.0:32801->7777/tcp                 
service-user    docker-entrypoint.sh npm start   Up      0.0.0.0:32803->7777/tcp                 
tap-db          docker-entrypoint.sh mongo ...   Up      0.0.0.0:27017->27017/tcp                
tap-nginx       nginx -g daemon off;             Up      0.0.0.0:443->443/tcp, 0.0.0.0:80->80/tcp
```
In case you want to stop and remove the containers just run:

```bash
[PROJECT_PATH]> docker-compose down

Stopping tap-nginx     ... done
Stopping service-auth  ... done
Stopping service-user  ... done
Stopping service-audit ... done
Stopping tap-db        ... done
Removing tap-nginx     ... done
Removing service-auth  ... done
Removing service-user  ... done
Removing service-audit ... done
Removing tap-db        ... done
Removing network tap_default
[PROJECT_PATH]>
```

Configuration
-------------
By default services will listen on port `7777` into the docker compose network. But as they are not exposing any port, they will be only accesible trought port 80 thanks to a reverse proxy created into tap-nginx container. Default port could be changed using SERVICE_PORT=[HERE_YOUR_DESIRED_PORT]. But you should also change nginx ports configured into nginx.conf file (look for section: proxy_pass http://service-audit:7777). 

Service endpoints
-----------------
- POST: http://localhost/auth/login
- POST: http://localhost/auth/logout
- POST: http://localhost/api/user/status (supports 1 or more user status)
- POST: http://localhost/api/audit/report
- POST: http://localhost/api/audit/register

Development
-----------
In case you want to launch each service in your local machine it could be useful to add to your /etc/hosts file an alias entry for tap-db as it is the db host declared in .env file. Otherwhise you may use localhost instead of tap-db in .env file. 

```bash
127.0.0.1       tap-db
```

Migrations
----------
Bear in mind package.json is configured to launch migrations to pre-populate DB with users. In case you want to disable migrations just change start or start:dev scripts into package.json.

Watcher
-------
For convinience you may launch any service using npm run dev. That restarts DB migrations and use nodemon as file watcher in case you need to change any file and inmediately relaunch the service.  

Tests
-----
Tests where created using postman so you may import the collection present in test/bdd/TAP.postman_collection.json
