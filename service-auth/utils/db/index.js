const mongoose = require('mongoose');

const log = require('debug')('tap:util:db');
const config = require('../../config');
const stringify = require('../../utils/stringify');

mongoose.Promise = global.Promise;
// log('Using DB config: ', stringify(config));


let connection = mongoose.connect(config.database.uri, config.database.options).then(
  (db) => {
    /** ready to use. The `mongoose.connect()` promise resolves to mongoose instance. */
    log(
      `Successfully connected to uri:${config.database.uri}, dbName:${config.database.options.dbName}, a MongoDB cluster in ${
        config.env
      } mode.`,
    );
    return db;
  },
  (err) => {
    /** handle initial connection error */
    if (err.message.code === 'ETIMEDOUT') {
      log('Attempting to re-establish database connection.');
      connection = mongoose.connect(config.database.uri, config.database.options);
    } else {
      log(`Error while attempting to connect to database: uri: ${config.database.uri}, and DBName: ${config.database.options.dbName}`);
      log(err);
    }
  },
);

module.export = connection;
