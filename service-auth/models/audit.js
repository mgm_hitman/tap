const mongoose = require('mongoose');

const { Schema } = mongoose;
const timestamps = require('mongoose-timestamp');
const mongooseStringQuery = require('mongoose-string-query');

// eslint-disable-next-line no-unused-vars
const log = require('debug')('services:controllers:audit');

const AuditSchema = new Schema(
  {
    userId: {
      type: mongoose.Schema.ObjectId,
      ref: 'Users',
      index: true,
      required: true,
    },
  },
  { collection: 'audits' },
);

// eslint-disable-next-line func-names
AuditSchema.pre('save', function (next) {
  if (!this.isNew) {
    next();
  }

  next();
});

AuditSchema.plugin(timestamps);
AuditSchema.plugin(mongooseStringQuery);

AuditSchema.index({ userId: 1 });

module.exports = mongoose.model('Audit', AuditSchema);
