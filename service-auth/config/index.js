require('dotenv').config({ path: './.env' });

module.exports = {
  env: process.env.NODE_ENV || 'development',
  server: {
    host: process.env.SERVICE_HOST, // Papertrail Logging Host
    port: process.env.SERVICE_PORT, // Papertrail Logging Port
  },
  stream: {
    appId: process.env.STREAM_APP_ID, // Stream Credentials – https://getstream.io
    apiKey: process.env.STREAM_API_KEY, // Stream Credentials – https://getstream.io
    apiSecret: process.env.STREAM_API_SECRET, // Stream Credentials – https://getstream.io
  },
  jwt: {
    secret: process.env.JWT_SECRET_SIGNING_KEY,
    expiration: process.env.JWT_TOKEN_EXPIRATION,
  },
  database: {
    uri: `mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}/`,
    options: {
      user: process.env.DB_USER,
      pass: process.env.DB_PASS,
      dbName: process.env.DB_NAME,
      useNewUrlParser: true,
      useCreateIndex: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      autoIndex: false, // Don't build indexes
      reconnectTries: Number.MAX_VALUE, // Never stop trying to reconnect
      reconnectInterval: 500, // Reconnect every 500ms
      poolSize: 3, // Maintain up to 10 socket connections
      // If not connected, return errors immediately rather than waiting for reconnect
      bufferMaxEntries: 0,
      connectTimeoutMS: 10000, // Give up initial connection after 10 seconds
      socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
      family: 4, // Use IPv4, skip trying IPv6
    },
  },
};
