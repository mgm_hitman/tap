const jwt = require('jsonwebtoken');
const log = require('debug')('tap:middlewares:auth');
const config = require('../config');
// const stringify = require('../utils/stringify');

module.exports = (req, res, next) => {
  const { headers } = req;
  log('Starting token verification');
  if (headers && headers.authorization) {
    try {
      const token = headers.authorization.replace('Bearer ', '');
      req.token = token;
      req.user = jwt.verify(token, config.jwt.secret, { algorithm: ['HS512'], ignoreExpiration: true });
      if (req.user) {
        log('Token verification OK');
      }
    } catch (err) {
      log('Error verifing token, cause: ', err.message);
      return res.status(401).json({ status: 'fail', msg: 'Failed to authenticate token!' });
    }
  } else {
    return res.status(401).json({ status: 'fail', msg: 'Unauthorized' });
  }
  next();
};
