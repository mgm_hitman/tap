const Auth = require('../controllers/auth');

module.exports = (api) => {
  api.route('/auth/login').post(Auth.login);
  api.route('/auth/logout').post(Auth.logout);
};
