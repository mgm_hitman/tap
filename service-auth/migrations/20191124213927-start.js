// module.exports = {
//   async up(db, client) {
//     // TODO write your migration here.
//     // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
//     // Example:
//     // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: true}});
//   },

//   async down(db, client) {
//     // TODO write the statements to rollback your migration (if possible)
//     // Example:
//     // await db.collection('albums').updateOne({artist: 'The Beatles'}, {$set: {blacklisted: false}});
//   },
// };

require('dotenv').config({ path: './.env' });
const log = require('debug')('tap:migrations:bootstrap');
const connection = require('../utils/db');
const User = require('../models/user');

module.exports = {
  async up() {
    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:

    const users = [
      {
        email: 'oteroleonardo@gmail.com',
        password: 'password',
        role: 'admin',
        firstname: 'Leonardo',
        lastname: 'Otero',
        cuil: '20-22650138-0',
        status: 1,
        plan: 1000,
        active: true,
      },
      {
        email: 'daniel.peralta@gmail.com',
        password: 'password',
        role: 'user',
        firstname: 'Daniel',
        lastname: 'Peralta',
        cuil: '20-22650139-0',
        status: 2,
        plan: 5000,
        active: true,
      },
      {
        email: 'santiago.libarona@gmail.com',
        password: 'password',
        role: 'user',
        firstname: 'Santiago',
        lastname: 'Libarona',
        cuil: '20-22650140-0',
        status: 3,
        plan: 500,
        active: true,
      },
      {
        email: 'carlos.estevez@gmail.com',
        password: 'password',
        role: 'user',
        firstname: 'Carlos',
        lastname: 'Estevez',
        cuil: '20-22650141-0',
        status: 4,
        plan: 1000,
        active: true,
      },
      {
        email: 'pedro.finkelstein@gmail.com',
        password: 'password',
        role: 'user',
        firstname: 'Pedro',
        lastname: 'Finkelstein',
        cuil: '20-22650142-0',
        status: 5,
        plan: 1000,
        active: false,
      },
    ];
    const result = await User.insertMany(users)
      .catch((err) => {
        if (err) throw err;
      });
    log('documents inserted in users collection:', result.length);
    await User.db.close();
  },

  async down() {
    await User.collection.drop();
    log('users collection droped');

    await User.db.close();
  },
};
