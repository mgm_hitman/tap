const Audit = require('../controllers/audit');

module.exports = (api) => {
  api.route('/api/audit/report').post(Audit.report);
  api.route('/api/audit/register').post(Audit.register);
};
