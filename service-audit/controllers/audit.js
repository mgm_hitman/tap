const log = require('debug')('tap:controllers:audit');
const Audit = require('../models/audit');
const stringify = require('../utils/stringify');

exports.report = async (req, res) => {
  const { role } = req.user;
  const { userId } = req.body;
  log('Caller user role: ', stringify(role));
  log('Caller user: ', stringify(req.user));
  log(`report starting for user: ${userId}`);

  if (role !== 'admin') {
    const message = `Admin role es required for this operation and your role is: ${role}`;
    log(message);
    return res.status(403).json({ message });
  }

  const used = await Audit.find({ userId }).countDocuments()
    .catch((e) => {
      log(`Error in report while retrieving API service usage for user: ${userId} , cause: `, e);
      return { error: { code: 400, message: 'Bad request' } };
    });

  if (used.error) {
    return res.status(used.error.code).json({ message: used.error.code });
  }

  return res.status(200).json({ used });
};

exports.register = async (req, res) => {
  const { role } = req.user;
  const { userId } = req.body;
  log('Caller user role: ', stringify(role));
  log('Caller user: ', stringify(req.user));
  log(`register starting for user: ${userId}`);

  if (role !== 'admin') {
    const message = `Admin role is required for this operation and your role is: ${role}`;
    log(message);
    return res.status(403).json({ message });
  }

  const result = await Audit.create({ userId })
    .catch((e) => {
      log(`Error in register while creating record in DB for user: ${userId} , cause: `, e);
      return { error: { code: 400, message: 'Bad request' } };
    });

  if (result.error) {
    return res.status(result.error.code).json({ message: result.error.code });
  }


  return res.status(200).json({ result });
};
