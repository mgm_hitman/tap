require('dotenv').config({ path: './.env' });
const log = require('debug')('tap:migrations:start');
const connection = require('../utils/db');
const Audit = require('../models/audit');

module.exports = {
  async up() {
    // TODO write your migration here.
    // See https://github.com/seppevs/migrate-mongo/#creating-a-new-migration-script
    // Example:

    const audits = [
      {
        userId: '5ddb843fb8eeab0041d0c3cd',
      }
    ];
    // const result = await Audit.insertMany(audit)
    //   .catch((err) => {
    //     if (err) throw err;
    //   });
    // log('documents inserted in audit collection:', result.length);
    await Audit.db.close();
  },

  async down() {
    await Audit.collection.drop();
    log('audit collection droped');

    await Audit.db.close();
  },
};
