/* eslint-disable global-require */
require('dotenv').config({ path: './.env' });
const fs = require('fs');
const path = require('path');
const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const compression = require('compression');
const logger = require('morgan');
const log = require('debug')('tap:service');

// eslint-disable-next-line no-console
log.log = console.log.bind(console); // don't forget to bind to console!

const auth = require('./middlewares/auth.js');
const config = require('./config');

const api = express();

api.use(cors());

api.use(compression());

api.use(bodyParser.urlencoded({ extended: true }));
api.use(bodyParser.json());


api.use((err, req, res, next) => {
  if (err.name === 'UnauthorizedError') {
    log(err);
    return res.status(401).send('Missing authentication credentials.');
  }
  next();
});

api.use(logger('dev'));
api.use('/api', auth);

api.listen(config.server.port, (err) => {
  log('Initializing api.listener');
  if (err) {
    log(err);
    process.exit(1);
  }
  // eslint-disable-next-line global-require
  require('./utils/db');
  log('Started DB connector');

  log(
    `API Service is now running on port ${config.server.port} in ${config.env} mode`,
  );
  fs.readdirSync(path.join(__dirname, 'routes')).forEach((file) => {
    log(`Adding ${file} to router`);
    // eslint-disable-next-line import/no-dynamic-require
    require(`./routes/${file}`)(api);
  });
});

module.exports = api;
